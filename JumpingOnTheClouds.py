c = [0, 0, 1, 0, 0, 1, 1, 0]
j = 2
def jumpingOnClouds1(c, j):
  hm = 100
  for a in range(0, len(c), j):
    if c[a] == 1:
      hm = hm - 1 - 2
    elif c[a] == 0:
      hm = hm - 1
  print(hm)

def jumpingOnClouds(c, k):
    energy=100
    pos=0
    n = len(c)
    while True:
        pos=(pos+k)%n
        if c[pos]==1:
            energy-=3
        else:
            energy-=1
        
        if pos==0:
            break
    print(energy)

jumpingOnClouds(c, j)

'''

ada "N" clouds dan jump distance "K" dan energy "E" = 100;

Pelompat dapat menggunakan 1 unit energy untuk membuat lompatan.

Pada thundercloud, energy pelompat berkurang 2

Karena n % k = 0 (Pelompat hanya akan berputar sekali)

Misal, N = 8; K = 2

clouds: 0 0 1 0 0 1 1 0

Pertama hitung berapa lompatan yang diperlukan: N / K = 8 / 2 = 4, ini artinya setelah pelompat berputar, energy akan menjadi: E - (N/K) = 100 - 4 = 96. (Pada tahap ini kita tidak peduli apakah awannya thundercloud atau bukan)

Setelah itu kita cek clouds, dimulai dari index 0, dan lompat 0+k, 0+k+k ... dan seterusnya... jika clouds thundercloud, energy berkurang 2 units.

index = 0; index < n; index = index + k
if cloud[index] == 1 //thundercloud// { energy = energy - 2 }

0 1 2 3 4 5 6 7

0 0 1 0 0 1 1 0
X - X - X - X -

Lompat di 2 thundercloud, jadi:

Energy = Energy - (thundercloud * 2 unit) = 96 - (2 * 2) = 92

'''