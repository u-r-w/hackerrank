d = 3
arr = [1, 2, 4, 5, 7, 8, 10]

from OpenTestCase import openTestCase 
def beautifulTriplets1(d, arr):
  arr = sorted(arr, reverse=True)
  e = arr[0]
  f = arr[0]
  g = arr[0]
  for i in range(len(arr)):
    if e - arr[i] == d:
      f = arr[i]
      print(e, f, g)
    if f - arr[i] == d:
      g = arr[i]
      print(e, f, g)

# 10 10 10
# 10 10 10
# 10 7 10
# 10 7 10
# 10 7 4
# 10 7 4
# 10 7 4

def beautifulTriplets2(d, arr):
  arr = sorted(arr, reverse=True)
  count = 0
  e = arr[0]
  f = arr[0]
  g = arr[0]
  for j in range(len(arr)):
    for i in range(len(arr)):
      if e - arr[i] == d:
        f = arr[i]
        print(e, f, g)
      if f - arr[i] == d:
        g = arr[i]
        print(e, f, g)
    if e - f == d and f - g == d:
      print(e, f, g, "<=")
      count += 1
    e = arr[0 + j]
    f = arr[0 + j]
    g = arr[0 + j]

  print(count) 

# 10 7 10
# 10 7 4
# 10 7 10
# 10 7 4
# 8 5 8
# 8 5 2
# 7 4 7
# 7 4 1
# 5 2 5
# 4 1 4

def beautifulTriplets3(d, arr):
  arr = sorted(arr, reverse=True)
  count = 0
  e = arr[0]
  f = arr[0]
  g = arr[0]
  for j in range(len(arr)):
    for i in range(j, len(arr)):
      for k in range(j, len(arr)):
        if e - arr[i] == d:
          f = arr[i]
          print(e, f, g)
        if f - arr[i] == d:
          g = arr[i]
          print(e, f, g)
        e = arr[0 + j]
        f = arr[0 + i]
        g = arr[0 + k]
        if e - f == d and f - g == d:
          print(e, f, g, "<=")
          count += 1

  print(count) 

def beautifulTriplets(d, arr):
  arr = sorted(arr, reverse=True)
  count = 0
  for j in range(len(arr)):
    for i in range(j, len(arr)):
      for k in range(j, len(arr)):
        e = arr[0 + j]
        f = arr[0 + i]
        g = arr[0 + k]
        if e - f == d and f - g == d:
          print(e, f, g, "<=")
          count += 1

  print(count) 


#a = openTestCase()
# beautifulTriplets(d, a) 


# n, d = map(int , raw_input().split(" "))
# a = map(int , raw_input().split(" "))
a = [1,2,4,5,7,8,9,10,12, 13,14]
#flag = [0] * (2 * 10000 + d + 1)
def smartTriplets1() :
  flag = [0] * (2 * max(a) + d + 1)
  
  #print(flag)
  for val in a:
      #print( val )
      flag[val] = 1
  #print( flag )
  
  ans = 0
  for val in a:
      print ( "# ", val, end=' -> ' )
      if flag[val + d] and flag[val + d + d]:  #all three numbers required to form a triplet exists
        i2 = flag[val+d]
        i3 = flag[val+d+d]
        #print(a[i2],a[i3], val)
        print( val, val+d, val+d+d)
        ans = ans + 1
      else :
        print( "")
  print(ans)

def smartTriplets2() :
  flag = [False] * ( max(a) + (2*d) ) # 2*d untuk menjaga val+d+d 
  
  print(len(flag), flag)
  for val in a:
      flag[val] = True
  print("")
  print(len(flag), flag)
  ans = 0
  for val in a:
      print ( "# ", val, end=' -> ' )
      if flag[val + d] and flag[val + d + d]:  #all three numbers required to form a triplet exists
        print( val, val+d, val+d+d)
        ans = ans + 1
      else :
        print( "")
  print(ans)

def smartTriplets4() :
  flag = [False] * ( max(a) + (2*d) ) # 2*d untuk menjaga val+d+d 
  
  print(len(flag), flag)
  for val in a:
      flag[val] = True
  print("")
  print(len(flag), flag)
  ans = 0
  for val in a:
      print ( "# ", val, end=' -> ' )
      if flag[val + d] and flag[val + d + d]:  #all three numbers required to form a triplet exists
        print( val, val+d, val+d+d)
        ans = ans + 1
      else :
        print( "")
  print(ans)

a = [1,2,4,5,7,8,9,10,12, 13,14, 100, 103,106]

def smartTriplets3() : # di beberapa bahasa pemrograman, bilangan ASLI (1,2,..) = TRUE, 0 = FALSE 
  flag = [0] * ( max(a) + (2*d) ) # 2*d untuk menjaga val+d+d 
  print(len(flag), flag)
  for val in a:
      flag[val] = val
  print("")
  print(len(flag), flag)
  print("")
  print(a)
  ans = 0
  for val in a:
      print ( "# ", val, end=' -> ' )
      if flag[val + d] and flag[val + d + d]:  # if bilangan ASLI == TRUE
        print( val, val+d, val+d+d)
        ans = ans + 1
      else :
        print( "")
  print(ans)

smartTriplets3()



class List:
  def __init__(self, c=None, n=None):
    self.c = c
    self.n = n
  def __str__(self):
    return self.c

def simpleLinkList() :
  l1 = List("Umar")
  l2 = List("Qonita")
  l3 = List("Nafila")
  # print(l1)
  
  l = l1
  l1.n = l2
  while l is not None:
    print(l)
    l = l.n

class IntList:
  def __init__(self, c=None, n=None):
    self.c = c
    self.n = n
  def __str__(self):
    return str(self.c)

def beautifulTriplets_List( a) : # teori/gambar lihat di buku halaman 27
#  a = openTestCase()
  a = sorted( a )
  arrlis = map( IntList, a ) # mencopy isi a sebagai array integer menjadi map of IntList ke dlm var arrlis
  b = list( arrlis ) # b adalah array of IntList
  print( type(arrlis), type(b), type(b[0]))
  for i in range(len(b)-1,-1,-1) : # jalan dari kanan ke kiri
#    print( b[i], b[i].c, b[i].n )
    for j in range( i-1, -1,-1 ) :
      if( b[i].c - 3 == b[j].c ) : # 3 adalah nilai d, jarak antar pengurangan 
        b[j].n = b[i]  # sambungkan panah merah dengan panah biru
  ans = 0
  for i in range(len(b)) : # hitung siapa yg punya sambungan lebih/sama dengan 3
    print( b[i],  b[i].n , end=  " ")
    l = b[i]
    c = 0
    while (l is not None) and ( c < 3 ) :
      l = l.n
      c = c + 1
    if c >= 3 :
      print( " result ", c )
      ans = ans + 1
    else :
      print( " ")
  print( "return ", ans )
  return ans  
    
from time import time
start = time()
beautifulTriplets_List( a )
end = time()
print( "Waktu eksekusi ", end - start , " detik")
# concept https://www.w3schools.com/python/showpython.asp?filename=demo_ref_map
