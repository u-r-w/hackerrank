# h = 1
# while h < n, h = 3*h + 1
# while h > 0,
#     h = h / 3
#     for k = 1:h, insertion sort a[k:h:n]
#     → invariant: each h-sub-array is sorted
# end

# a = [6, 4, 5, 7, 2, 1, 3]
# n = len(a) - 1

# from InsertionSort import insertion1

# h = 1
# while h < n:
#   h = 3 * h + 1
# print("h",h)
# while h > 0:
#     h = int(h / 3)
#     print("h", h)
#     for k in range(1, h): 
#       insertion1(a[k:h], h)
#       insertion1(a[h:n], n)

from termcolor import colored

step = 0
def printcolor( step, a,i, sublistcount, label ) : # TODO: Step 18 harusnya bukan ngga dimerahin semua karena sudah urut
  # TODO: Kenapa ketika mencapai langkah 12 (sudah ter-sorting ) masih lanjut proses pengurutan?
  for x in range( len(a)) : 
    if( x == i  ) : # apakah angka yang sedang di periksa sama dengan i & j 
      if True == True:
        print( colored(a[x], 'red') , end=' ')
      else:
        print( colored(a[x], 'blue') , end=' ')
    else :
      print( a[x], end=' ')
  print( '#', step, a, " #", sublistcount , label )


def shellSort(alist):
    global step
    sublistcount = len(alist)//2
    while sublistcount > 0:

      for i in range(sublistcount):
        gapInsertionSort(alist,i,sublistcount)
        step += 1
        printcolor( i, alist,i, step , "shell")

   #   print("After increments of size",sublistcount, "The list is", printcolor(alist))

      sublistcount = sublistcount // 2

def gapInsertionSort(alist,start,gap):
    global step
    for i in range(start+gap,len(alist),gap):

        currentvalue = alist[i]
        position = i

        while position>=gap and alist[position-gap]>currentvalue:
            alist[position]=alist[position-gap]
            position = position-gap
            step = step + 1
            printcolor( i, alist, position, step , "gapIns" )

        alist[position]=currentvalue

alist = [6, 4, 5, 7, 0, 2, 1, 8,3,9 ]
shellSort(alist)
print(alist)
