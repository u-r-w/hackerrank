from termcolor import colored
from time import sleep

a = [3, 4, 5, 7, 2, 1, 6]
a = [6, 4, 5, 7, 2, 1, 3]
n = len(a) - 1
# for i = 1:n,
#     swapped = false
#     for j = n:i+1,
#         if a[j] < a[j-1],
#             swap a[j,j-1]
#             swapped = true
#     → invariant: a[1..i] in final position
#     break if not swapped
# end
def pcolor( mystring ) :
  print(colored( mystring, 'red'))
  
def swap(x, y):
  # a[x] , a[y] = a[y] , a[x]
  temp = a[x]
  a[x] = a[y]
  a[y] = temp
  return x , y
# print(a)
# b = swap(0, 1)
# print(a)
def bubble1() :
  print( "#Step array i j swapped color")
  step = 0
  for i in range(n):
    swapped = False
    for j in range(n, i, -1):
      # print("i = ", i, "j", j)
      step += 1
      if a[j] < a[j-1]:
        swap(j, j-1)
        swapped = True
  
      print("#",step, a, i, j, swapped , end=' ')
      s = str( a[j-1]) + " " + str( a[j])
      pcolor( s  )
  
    if not swapped:
      break
  print(a)

def printcolor1( step, a, i, j, swapped ) :
  print( step, a,i,j, swapped, end=" ")

def printcolor( step, a, i, j, swapped ) : # TODO: Step 18 harusnya bukan ngga dimerahin semua karena sudah urut
  # TODO: Kenapa ketika mencapai langkah 12 (sudah ter-sorting ) masih lanjut proses pengurutan?
  for x in range( len(a)) : 
    if( x == i or x == j ) : # apakah angka yang sedang di periksa sama dengan i & j 
      if swapped == True:
        print( colored(a[x], 'red') , end=' ')
      else:
        print( colored(a[x], 'blue') , end=' ')
    else :
      print( a[x], end=' ')
  print( step, a,i,j, swapped, end=" ")

  print(step, "")
def printcolor_sleep(step, a, i, j, swapped ):
  print("\r", end="")
  # TODO: Kenapa ketika mencapai langkah 12 (sudah ter-sorting ) masih lanjut proses pengurutan?
  for x in range( len(a)) : 
    sleep(0.1)
    if( x == i or x == j ) : # apakah angka yang sedang di periksa sama dengan i & j 
      if swapped == True:
        print( colored(a[x], 'red') , end=' ')
      else:
        print( colored(a[x], 'blue') , end=' ')
    else :
      print( a[x], end=' ')

def bubble2() :
  step = 0
  for i in range(n):
    swapped = False
    for j in range(n, i, -1):
      # print("i = ", i, "j", j)
      step += 1
      if a[j] < a[j-1]:
        swap(a.index(a[j]), a.index(a[j-1]))
        swapped = True
  
      printcolor( step, a, j - 1, j, swapped )
  
    if not swapped:
      break

#bubble2()