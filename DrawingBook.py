n = 5
p = 4

def pageCount(n, p):
  page_in_book = p//2
  total_pages = n//2
  print("total_pages = ", total_pages)
  print("page_in_book = ", page_in_book)
  from_front = page_in_book
  from_back = total_pages - page_in_book
  print(min(from_front, from_back))
pageCount(n, p)
